<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="bulleted-lists" xml:lang="pl">

  <info>
    <link type="guide" xref="editing-notes#note-content"/>
    <desc>Używanie list wypunktowanych</desc>
    <license><p>Licencji GNU Wolnej Dokumentacji (GFDL), wersja 1.1</p></license>
    <credit type="author">
      <name>Pierre-Yves Luyten</name>
      <years>2011</years>
    </credit>
    <credit type="editor">
      <name>Aurimas Cernius</name>
      <years>2011</years>
    </credit>
    <credit type="editor">
      <name>Adam Dingle</name>
      <email>adam@yorba.org</email>
      <years>2013</years>
    </credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2018-2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2018-2019</mal:years>
    </mal:credit>
  </info>

<title>Listy wypunktowane</title>

<section id="begin-a-bulleted-list"><title>Rozpoczynanie listy wypunktowanej</title>
<p>Aby rozpocząć listę wypunktowaną:</p>
<list>
<item><p>Wybierz <gui>Wypunktowanie</gui> z menu <gui>Tekst</gui>.</p></item>
<item><p>Zacznij wiersz od jednego myślnika (<key>-</key>) i spacji. Następnie wpisz jakiś tekst i naciśnij klawisz <key>Enter</key>.</p></item>
<item><p>Naciśnij kombinację klawiszy <keyseq><key>Alt</key><key>W prawo</key></keyseq>.</p></item>
</list>
</section>

<section id="end-a-bulleted-list"><title>Kończenie listy wypunktowanej</title>
<p>Aby zakończyć listę wypunktowaną:</p>
<list>
<item><p>Wybierz <gui>Wypunktowanie</gui> z menu <gui>Tekst</gui>.</p></item>
<item><p>Naciśnij klawisz <key>Enter</key> na pustym wypunktowanym wierszu.</p></item>
<item><p>Wybierz <gui>Zmniejsz wcięcie</gui> z menu Tekst kilka razy, aż obecny wiersz nie jest już częścią wypunktowanej listy.</p></item>
<item><p>Wielokrotnie naciśnij klawisze <keyseq><key>Shift</key><key>Tab</key></keyseq>, aż obecny wiersz nie jest już częścią wypunktowanej listy.</p></item>
<item><p>Wielokrotnie naciśnij klawisze <keyseq><key>Alt</key><key>W lewo</key></keyseq>, aż obecny wiersz nie jest już częścią wypunktowanej listy.</p></item>
</list>

</section>
<section id="increase-indentation">
<title>Zwiększanie wcięć</title>
<p>Aby zwiększyć wcięcie wiersza na wypunktowanej liście za pomocą myszy:</p>
<list><item><p>Wybierz <gui>Zwiększ wcięcie</gui> z menu Tekst.</p></item></list>
<p>Aby zwiększyć wcięcie wiersza na wypunktowanej liście za pomocą klawiatury:</p>
<list>
<item><p>Naciśnij klawisz <key>Tab</key>.</p></item>
<item><p>Naciśnij kombinację klawiszy <keyseq><key>Alt</key><key>W prawo</key></keyseq>.</p></item>
</list>
</section>

<section id="decrease-indentation">
<title>Zmniejszanie wcięć</title>
<p>Aby zmniejszyć wcięcie wiersza na wypunktowanej liście za pomocą myszy:</p>
<list><item><p>Wybierz <gui>Zmniejsz wcięcie</gui> z menu Tekst.</p></item></list>
<p>Aby zmniejszyć wcięcie wiersza na wypunktowanej liście za pomocą klawiatury:</p>
<list>
<item><p>Naciśnij klawisze <keyseq><key>Shift</key><key>Tab</key></keyseq>.</p></item>
<item><p>Naciśnij klawisze <keyseq><key>Alt</key><key>W lewo</key></keyseq>.</p></item>
</list>
</section>

</page>
