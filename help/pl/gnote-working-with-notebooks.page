<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="working-with-notebooks" xml:lang="pl">


  <info>
    <link type="guide" xref="index#organizing-notes"/>
    <desc>Organizowanie notatek</desc>
    <license><p>Licencji GNU Wolnej Dokumentacji (GFDL), wersja 1.1</p></license>
    <credit type="author">
      <name>Pierre-Yves Luyten</name>
      <years>2011</years>
    </credit>
    <credit type="editor">
      <name>Aurimas Cernius</name>
      <years>2011-2012, 2019</years>
    </credit>
    <credit type="editor">
      <name>Adam Dingle</name>
      <email>adam@yorba.org</email>
      <years>2013</years>
    </credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2018-2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2018-2019</mal:years>
    </mal:credit>
  </info>

  <title>Działanie na notatnikach</title>
    <p>Notatniki umożliwiają grupowanie powiązanych notatek. Można dodać notatkę do notatnika w głównym oknie lub w oknie redakcji notatki.</p>

    <section id="creating-notebooks"><title>Tworzenie notatników</title>
      <p>Aby utworzyć nowy notatnik:</p>
      <steps>
        <item>
          <p>Kliknij przycisk menu w głównym oknie i wybierz <gui>Nowy notatnik</gui>.</p>
        </item>
        <item>
          <p>Wpisz nazwę nowego notatnika.</p>
        </item>
      </steps>
      <p>Lub:</p>
      <steps>
        <item>
          <p>W przeglądzie notatki kliknij prawym przyciskiem myszy w panelu po lewej i wybierz <gui>Nowa…</gui>.</p>
        </item>
        <item>
          <p>Wpisz nazwę nowego notatnika.</p>
        </item>
      </steps>
      <figure>
        <title>Nowy notatnik z menu kontekstowego</title>
        <media type="image" src="figures/add-notebook-search.png" mime="image/png" style="right">
        <p>Nowy notatnik</p>
      </media>
      </figure>  
    </section>

    <section id="adding-notes-to-a-notebook"><title>Dodawanie notatek do notatnika</title>
      <p>Można dodać notatkę do notatnika z głównego okna lub bezpośrednio w notatce.</p>
      <p>Aby dodać notatkę do notatnika z głównego okna:</p>
      <list>
        <item><p>Przeciągnij notatki na notatnik po lewej za pomocą myszy.</p></item>
        <item><p>Kliknij notatnik, kliknij listę notatek prawym przyciskiem myszy i wybierz <gui>Nowa</gui> z menu kontekstowego. Nowa notatka zostanie utworzona i dodana do notatnika.</p></item>
      </list>
      <p>Podczas piania notatki można dodać notatkę bezpośrednio do istniejącego notatnika za pomocą menu <guiseq><gui>Działania</gui><gui>Notatnik</gui></guiseq>.</p>
      <figure>
        <title>Dodawanie notatek do notatnika</title>
        <media type="image" src="figures/add-to-notebook.png" mime="image/png" style="right"><p>Dodawanie do notatnika.</p></media>
      </figure>
      <p>Aby usunąć notatkę z notatnika:</p>
      <list>
        <item><p>Przenieś notatkę do <gui>Niewypełnionych notatek</gui> w głównym oknie.</p></item>
        <item><p>Wybierz opcję <gui>Brak notatnika</gui> z menu, które zostanie wyświetlone po kliknięciu <guiseq><gui>Działania</gui><gui>Notatnik</gui></guiseq> w otwartej notatce.</p></item>
      </list>
    </section>

    <section id="creating-notebook-templates"><title>Tworzenie szablonu</title>
      <p>Po utworzeniu notatnika można utworzyć szablon dla nowych notatek tworzonych w tym notatniku.</p>
      <steps>
        <item><p>W głównym oknie kliknij notatnik prawym przyciskiem myszy i wybierz <gui>Otwórz szablon notatki</gui>.</p>
        </item>
        <item><p>Otworzy to nowy szablon. Tekst wpisany w tej notatce będzie we wszystkich notatkach utworzonych w tym notatniku.</p></item>
      </steps>
      <figure>
        <title>Tworzenie szablonu</title>
        <media type="image" src="figures/note-template.png" mime="image/png" style="right"><p>Szablon.</p></media>
      </figure>
      <p><link xref="gnote-template-notes"/> zawiera więcej informacji.</p>
    </section>

  <section id="deleting-a-notebook"><title>Usuwanie notatnika</title>
    <p>Usunięcie notatnika nie usunie żadnych notatek obecnie znajdujących się w notatniku. Po usunięciu notatnika jego notatki nie będą powiązane z żadnym notatnikiem i będą wyświetlane w sekcji <gui>Niewypełnione notatki</gui> w głównym oknie.</p>
    <p>Aby usunąć notatnik, przejdź do głównego okna oraz:</p>
    <list>
      <item><p>Kliknij notatnik prawym przyciskiem myszy i wybierz <gui>Usuń notatnik</gui>.</p></item>
      <item><p>Usuń wszystkie notatki z notatnika. Notatnik zostanie automatycznie usunięty po następnym uruchomieniu programu <app>Gnote</app>.</p></item>
    </list>
    <figure>
      <title>Usuwanie notatnika z głównego okna</title>
        <media type="image" src="figures/delete-notebook.png" mime="image/png" style="right">
          <p>Usuwanie notatnika.</p>
        </media>
    </figure>
  </section>

  <section id="special-notebooks"><title>Specjalne notatniki</title>
    <p><app>Gnote</app> ma kilka specjalnych notatników:</p>
    <list>
      <item><p><gui>Aktywne</gui>: notatki otwarte w obecnej sesji programu <app>Gnote</app></p></item>
      <item><p><gui>Wszystkie</gui>: wszystkie notatki poza szablonami</p></item>
      <item><p><gui>Ważne</gui>: notatki oznaczone jako ważne</p></item>
      <item><p><gui>Niewypełnione</gui>: wszystkie notatki nieznajdujące się w notatniku utworzonym przez użytkownika</p></item>
    </list>
  </section>

</page>
