<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:e="http://projectmallard.org/experimental/" type="guide" id="editing-notes" xml:lang="cs">

  <info>
    <link type="guide" xref="index#managing-notes"/>
    <license><p>GNU Free Documentation License (GFDL), verze 1.1</p></license>
    <desc>Úprava obsahu poznámek</desc>
    <credit type="author">
      <name>Pierre-Yves Luyten</name>
      <years>2011</years>
    </credit>
    <credit type="editor">
      <name>Aurimas Cernius</name>
      <years>2011 – 2013</years>
    </credit>
    <credit type="editor">
      <name>Adam Dingle</name>
      <email>adam@yorba.org</email>
      <years>2013</years>
    </credit>
  </info>

  <title>Psaní poznámek</title>

    <p>Když otevřete poznámku, <app>Gnote</app> nahradí přehled poznámek obsahem toto poznámky.</p>
    <p>Nástrojová lišta v horní části okna nyní bude obsahovat položky vztahující se k poznámce. Pod nástrojovou lištou je oblast s obsahem poznámky.</p>
    <figure>
      <title>Okno <app>Gnote</app> zobrazující poznámku</title>
      <desc>Poznámka <app>Gnote</app></desc>
      <media type="image" src="figures/gnote-new-note.png" mime="image/png" style="right">
        <p>Úprava poznámky <app>Gnote</app>.</p>
      </media>
    </figure>

    <p>Když chcete poznámku upravit, klikněte kdekoliv do ní a začněte psát. První řádek je považován za název poznámky. Ve výchozím stavu je vyplněn textem „Nová poznámka (číslo)“. Pokud chcete nadpis změnit, klikněte na první řádek a použijte klávesnici ke změně.</p>
    <p>K formátování textu poznámky použijte tlačítko <gui>Text</gui>. To zobrazí nabídku s několika příkazy:</p>

    <list>
      <item><title>Zpět</title>
        <p>Funkce <gui>Zpět</gui> vrácí změny provedené ve vaší poznámce v aktuálním sezení. Pokud chcete vrátit zpět poslední změny pomocí klávesnice, zmáčkněte <keyseq><key>Ctrl</key><key>Z</key></keyseq>.</p>
      </item>
      <item><title>Znovu</title>
        <p>Funkce <gui>Znovu</gui> se používá k vrácení změn, které jste zrušili pomocí funkce <gui>Zpět</gui>. Pokud chcete znovu vrátit změny pomocí klávesnice, zmáčkněte <keyseq><key>Shift</key><key>Ctrl</key><key>Z</key></keyseq>.</p>
      </item>
      <item><title>Tučné</title>
        <p>Pokud chcete mít některou část textu vaší poznámky tučně, tak ji vyberte a po té v nabídce <gui>Text</gui> zvolte příkaz <gui>Tučné</gui> nebo zmáčkněte <keyseq><key>Ctrl</key><key>B</key></keyseq>.</p>
      </item>
      <item><title>Kurzíva</title>
        <p>Pokud chcete mít některou část textu vaší poznámky v kurzívě, tak ji vyberte a po té v nabídce <gui>Text</gui> zvolte příkaz <gui>Kurzíva</gui> nebo zmáčkněte <keyseq><key>Ctrl</key><key>I</key></keyseq>.</p>
     </item>
     <item><title>Přeškrtnuté</title>
       <p>Přeškrtnutí umístí vodorovnou čáru přes vybraný text. Pokud chcete přeškrtnutí použít, tak vyberte text a v nabídce <gui>Text</gui> zvolte příkaz <gui>Přeškrtnuté</gui> nebo zmáčkněte <keyseq><key>Ctrl</key><key>S</key></keyseq>.</p>
     </item>
     <item><title>Zvýraznění</title>
       <p>Zvýraznění umístí pod vybraný text žluté pozadí. Pokud chcete zvýraznění použít, tak vyberte text a v nabídce <gui>Text</gui> zvolte příkaz <gui>Zvýraznění</gui> nebo zmáčkněte <keyseq><key>Ctrl</key><key>H</key></keyseq>.</p>
     </item>
     <item><title>Velikost písma</title>
       <p>V současnosti jsou v této části nabídky k dispozici čtyři volby: Malé, Normální, Velké, Obrovské. Každá z těchto voleb představuje velikost písma použitou na vybraný text poznámky. Pokud chcete velikost písma změnit, vyberte text a zvolte jednu z možností <gui>Malé</gui>, <gui>Normální</gui>, <gui>Velké</gui>, <gui>Obrovské</gui>.</p>
     </item>
     <item><title>Odrážky</title>
       <p>Volbu příkazu <gui>Odrážky</gui> v nabídce <gui>Text</gui> začnete nebo ukončíte seznam s odrážkami. Pokud se kurzor nachází uvnitř seznamu s odrážkami, jsou dostupné volby <gui>Zvětšit odsazení</gui> a <gui>Zmenšit odsazení</gui>.</p>
       <p>S kurzorem umístěným na řádku s odrážkou zvolte <gui>Zvětšit odsazení</gui>, a aktuální řádek se odsune doprava, nebo <gui>Zmenšit odsazení</gui>, a aktuální řádek se přisune doleva.</p>
       <p>Více informací o odrážkách podává kapitola <link xref="bulleted-lists"/>.</p>
     </item>
   </list>
   <p>Nástrojová lišta v horní části poznámky obsahuje vyhledávací tlačítko.Použijte jej k hledání textu v rámci aktuální poznámky. V dolní části poznámky se otevře malá vyhledávací lišta. Pro otevření této lišty z klávesnice zmáčkněte <keyseq><key>Ctrl</key><key>F</key></keyseq>. Zadejte text, který chcete najít, a zmáčkněte <key>Enter</key>. Nalezené shody se zvýrazní. Kliknutím na <gui>Následující</gui> se kurzor přesune na následující zvýrazněnou shodu. Kliknutím na <gui>Předchozí</gui> se přesune na předchozí zvýrazněnou shodu. Vyhledávací lištu zavřete opětovným kliknutím na vyhledávací tlačítko nebo zmáčknutím klávesy <key>Esc</key>.</p>

   <section id="note-content" style="2column">
     <title>Vylepšení obsahu poznámek</title>
   </section>

</page>
