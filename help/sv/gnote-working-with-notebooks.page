<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="working-with-notebooks" xml:lang="sv">


  <info>
    <link type="guide" xref="index#organizing-notes"/>
    <desc>Organisera dina anteckningar</desc>
    <license><p>GNU Free Documentation License (GFDL), Version 1.1</p></license>
    <credit type="author">
      <name>Pierre-Yves Luyten</name>
      <years>2011</years>
    </credit>
    <credit type="editor">
      <name>Aurimas Cernius</name>
      <years>2011-2012,2019</years>
    </credit>
    <credit type="editor">
      <name>Adam Dingle</name>
      <email>adam@yorba.org</email>
      <years>2013</years>
    </credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2006, 2007, 2008, 2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Josef Andersson</mal:name>
      <mal:email>josef.andersson@fripost.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

  <title>Arbeta med anteckningsblock</title>
    <p>Anteckningsblock låter dig gruppera relaterade anteckningar. Du kan lägga till en anteckning till ett anteckningsblock i huvudfönstret eller i redigeringsvyn för anteckningar.</p>

    <section id="creating-notebooks"><title>Skapa anteckningsblock</title>
      <p>För att skapa en nytt anteckningsblock:</p>
      <steps>
        <item>
          <p>Klicka på menyknappen i huvudfönstret och välj <gui>Nytt anteckningsblock</gui>.</p>
        </item>
        <item>
          <p>Ange ett namn för det nya anteckningsblocket.</p>
        </item>
      </steps>
      <p>Alternativ:</p>
      <steps>
        <item>
          <p>I översikten för anteckningar, högerklicka i vänstra panelen och välj <gui>Nytt...</gui>.</p>
        </item>
        <item>
          <p>Ange ett namn för det nya anteckningsblocket.</p>
        </item>
      </steps>
      <figure>
        <title>Nytt anteckningsblock från sammanhangsmeny</title>
        <media type="image" src="figures/add-notebook-search.png" mime="image/png" style="right">
        <p>Nytt anteckningsblock</p>
      </media>
      </figure>  
    </section>

    <section id="adding-notes-to-a-notebook"><title>Lägg till anteckningar till ett anteckningsblock</title>
      <p>Du kan lägga till en anteckning till ett anteckningsblock från huvudfönstret eller direkt från en anteckning.</p>
      <p>För att öppna en anteckning från huvudfönstret, gör ett av följande:</p>
      <list>
        <item><p>Dra anteckningen (eller flera) till ett anteckningsblock på vänster sida med hjälp av musen.</p></item>
        <item><p>Klicka på ett anteckningsblock, högerklicka i anteckningslistan och välj <gui>Ny</gui> från sammanhangsmenyn. En ny anteckning kommer att skapas och läggas till i anteckningsblocket.</p></item>
      </list>
      <p>När du skriver en anteckning kan du lägga till anteckningen direkt till ett befintligt anteckningsblock med menyn <guiseq><gui>Åtgärder</gui><gui>Anteckningsblock</gui></guiseq>.</p>
      <figure>
        <title>Lägg till anteckningar till ett anteckningsblock</title>
        <media type="image" src="figures/add-to-notebook.png" mime="image/png" style="right"><p>Lägg till i anteckningsblock.</p></media>
      </figure>
      <p>För att ta bort ett anteckningsblock, använd en av följande:</p>
      <list>
        <item><p>Flytta anteckningen till <gui>Ej arkiverade anteckningar</gui> i huvudfönstret.</p></item>
        <item><p>Välj alternativet <gui>Inget anteckningsblock</gui> från menyn som visas när du trycker ned <guiseq><gui>Åtgärder</gui><gui>Anteckningsblock</gui></guiseq> i en öppen anteckning.</p></item>
      </list>
    </section>

    <section id="creating-notebook-templates"><title>Skapa en mall</title>
      <p>Efter att du skapat ett anteckningsblock kan du skapa en mall för varje ny anteckning som du skapar i det anteckningsblocket.</p>
      <steps>
        <item><p>I huvudfönstret, högerklicka på ett anteckningsblock som du har skapat och välj <gui>Öppna anteckningsmall</gui>.</p>
        </item>
        <item><p>Detta kommer att öppna en ny anteckningsmall. Text skriven i denna anteckning kommer att visas i alla anteckningar skapade i detta anteckningsblock.</p></item>
      </steps>
      <figure>
        <title>Skapa en mall</title>
        <media type="image" src="figures/note-template.png" mime="image/png" style="right"><p>Anteckningsmall.</p></media>
      </figure>
      <p>För mer, se <link xref="gnote-template-notes"/>.</p>
    </section>

  <section id="deleting-a-notebook"><title>Ta bort ett anteckningsblock</title>
    <p>Borttagning av ett anteckningsblock kommer inte att ta bort några av de anteckningar som anteckningsblocket innehåller. Efter borttagning av ett anteckningsblock kommer inte anteckningarna att associeras med något anteckningsblock och kan ses genom att välja <gui>Ej arkiverade anteckningar</gui>.</p>
    <p>För att ta bort ett anteckningsblock, gå till huvudfönstret och utför en av följande:</p>
    <list>
      <item><p>Högerklicka på ett anteckningsblock och välj <gui>Ta bort anteckningsblock</gui>.</p></item>
      <item><p>Ta bort alla anteckningar från ett anteckningsblock. Anteckningsblocket kommer automatiskt att tas bort nästa gång <app>Gnote</app> startar om.</p></item>
    </list>
    <figure>
      <title>Ta bort anteckningar från huvudfönstret</title>
        <media type="image" src="figures/delete-notebook.png" mime="image/png" style="right">
          <p>Ta bort anteckningsblock.</p>
        </media>
    </figure>
  </section>

  <section id="special-notebooks"><title>Speciella anteckningsblock</title>
    <p><app>Gnote</app> har flera speciella anteckningsblock:</p>
    <list>
      <item><p><gui>Aktiva</gui>: anteckningar som har öppnats i nuvarande <app>Gnote</app>-session</p></item>
      <item><p><gui>Alla</gui>: alla anteckningar förutom mallar</p></item>
      <item><p><gui>Viktiga</gui>: anteckningar markerade som viktiga</p></item>
      <item><p><gui>Ej arkiverade</gui>: alla anteckningar som inte finns i ett användarskapat anteckningsblock</p></item>
    </list>
  </section>

</page>
