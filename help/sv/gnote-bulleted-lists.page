<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="bulleted-lists" xml:lang="sv">

  <info>
    <link type="guide" xref="editing-notes#note-content"/>
    <desc>Använda punktlistor</desc>
    <license><p>GNU Free Documentation License (GFDL), Version 1.1</p></license>
    <credit type="author">
      <name>Pierre-Yves Luyten</name>
      <years>2011</years>
    </credit>
    <credit type="editor">
      <name>Aurimas Cernius</name>
      <years>2011</years>
    </credit>
    <credit type="editor">
      <name>Adam Dingle</name>
      <email>adam@yorba.org</email>
      <years>2013</years>
    </credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2006, 2007, 2008, 2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Josef Andersson</mal:name>
      <mal:email>josef.andersson@fripost.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

<title>Punktlistor</title>

<section id="begin-a-bulleted-list"><title>Påbörja en punktlista</title>
<p>Du kan påbörja en punktlista genom en av följande metoder:</p>
<list>
<item><p>Välj <gui>Punkter</gui> från menyn <gui>Text</gui>.</p></item>
<item><p>Börja en rad med ett minustecken <key>-</key> följt av ett blanksteg. Skriv sedan lite text och tryck på <key>Retur</key>.</p></item>
<item><p>Tryck ned tangentkombinationen <keyseq><key>Alt</key><key>Högerpil</key></keyseq>.</p></item>
</list>
</section>

<section id="end-a-bulleted-list"><title>Avsluta en punktlista</title>
<p>Du kan avsluta en punktlista genom att göra ett av följande:</p>
<list>
<item><p>Välj <gui>Punkter</gui> från menyn <gui>Text</gui>.</p></item>
<item><p>Tryck ned <key>Retur</key> på en tom punktrad.</p></item>
<item><p>Välj <gui>Minska indrag</gui> från textmenyn i följd tills den aktuella raden inte längre är en del av punktlistan.</p></item>
<item><p>Tryck ned <keyseq><key>Skift</key><key>Tab</key></keyseq> upprepat tills den aktuella raden inte längre är en del av punktlistan.</p></item>
<item><p>Tryck ned <keyseq><key>Alt</key><key>Vänsterpil</key></keyseq> upprepat tills den aktuella raden inte längre är en del av punktlistan.</p></item>
</list>

</section>
<section id="increase-indentation">
<title>Öka indrag</title>
<p>För att öka radindrag i en punktlista med musen:</p>
<list><item><p>Välj <gui>Öka indrag</gui> från textmenyn.</p></item></list>
<p>För att öka radindrag i en punktlista med tangentbordet:</p>
<list>
<item><p>Tryck ned <key>Tab</key>tangenten.</p></item>
<item><p>Tryck ned tangentkombinationen <keyseq><key>Alt</key><key>Högerpil</key></keyseq>.</p></item>
</list>
</section>

<section id="decrease-indentation">
<title>Minska indrag</title>
<p>För att minska radindrag i en punktlista med musen:</p>
<list><item><p>Välj <gui>Minska indrag</gui> från textmenyn.</p></item></list>
<p>För att minska radindrag i en punktlista med tangentbordet:</p>
<list>
<item><p>Tryck ned tangentkombinationen <keyseq><key>Skift</key><key>Tab</key></keyseq>.</p></item>
<item><p>Tryck ned tangentkombinationen <keyseq><key>Alt</key><key>Vänsterpil</key></keyseq>.</p></item>
</list>
</section>

</page>
