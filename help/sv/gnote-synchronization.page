<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="synchronization" xml:lang="sv">

  <info>
    <link type="guide" xref="index#advanced"/>
    <desc>Synkronisera anteckningar</desc>
    <license><p>GNU Free Documentation License (GFDL), Version 1.1</p></license>
    <credit type="author">
      <name>Aurimas Cernius</name>
      <years>2012,2019</years>
    </credit>
    <credit type="editor">
      <name>Adam Dingle</name>
      <email>adam@yorba.org</email>
      <years>2013</years>
    </credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2006, 2007, 2008, 2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Josef Andersson</mal:name>
      <mal:email>josef.andersson@fripost.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

  <title>Synkronisering</title>
  <section id="description">
    <title>Om synkronisering</title>
    <p><app>Gnote</app> låter dig synkronisera anteckningar mellan flera maskiner så att du kan använda dem på flera enheter och arbeta med samma anteckningar på dem alla.</p>
    <p>För att använda synkronisering måste du först konfigurera det i <app>Gnote</app>s konfigurationsdialog.</p>
    <p>Anteckningar synkroniseras till en plats, gemensam för alla <app>Gnote</app>-installationer. Denna plats lagrar historiken över dina anteckningar. Vid synkronisering skickar varje instans av <app>Gnote</app> upp nya och uppdaterade anteckningar till denna plats, och hämtar nya och uppdaterade anteckningar från andra installationer. Synkroniseringsplatsen innehåller också information om borttagna anteckningar: under synkronisering publicerar <app>Gnote</app> alla borttagna anteckningar och tar bort de som har publicerats som sådana. På detta sätt kan flera <app>Gnote</app>-installationer hantera samma mängd av anteckningar.</p>
  </section>
  <section id="configuration" style="2column">
    <title>Konfigurera synkronisering</title>
  </section>
  <section id="usage">
    <title>Synkronisera</title>
    <p>För att synkronisera, klicka på menyknappen i huvudfönstret och välj sedan <gui>Synkronisera anteckningar</gui> från menyn. Detta objekt är endast tillgängligt om synkronisering har konfigurerats.</p>
    <p>Efter att du har valt menyobjektet <gui>Synkronisera anteckningar</gui> kommer en förloppsdialog som berättar när synkroniseringen är färdig att visas. Alla anteckningar kommer också att inaktiveras för redigering medan synkronisering pågår.</p>
    <figure>
      <title>Synkroniseringsförlopp</title>
      <media type="image" src="figures/gnote-sync-progress.png" mime="image/png"/>
    </figure>
  </section>

</page>
