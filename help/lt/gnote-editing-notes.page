<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:e="http://projectmallard.org/experimental/" type="guide" id="editing-notes" xml:lang="lt">

  <info>
    <link type="guide" xref="index#managing-notes"/>
    <license><p>GNU laisva dokumentacijos licencija (GFDL), versija 1.1</p></license>
    <desc>Raštelių turinio redagavimas</desc>
    <credit type="author">
      <name>Pierre-Yves Luyten</name>
      <years>2011</years>
    </credit>
    <credit type="editor">
      <name>Aurimas Cernius</name>
      <years>2011-2013</years>
    </credit>
    <credit type="editor">
      <name>Adam Dingle</name>
      <email>adam@yorba.org</email>
      <years>2013</years>
    </credit>
  </info>

  <title>Raštelių redagavimas</title>

    <p>Kai atversite raštelį, <app>Gnote</app> pakeis raštelių apžvalgą raštelio turiniu.</p>
    <p>Įrankinė lango viršuje dabar turės raštelio punktus. Žemiau bus raštelio turinio vieta.</p>
    <figure>
      <title><app>Gnote</app> langas, rodantis raštelį</title>
      <desc><app>Gnote</app> raštelis</desc>
      <media type="image" src="figures/gnote-new-note.png" mime="image/png" style="right">
        <p><app>Gnote</app> raštelio redagavimas.</p>
      </media>
    </figure>

    <p>Rašteliui redaguoti spauskite turinio vietoje ir rašykite. Pirmoji eilutė laikoma raštelio pavadinimu. Numatytai tai yra „Naujas raštelis (numeris)“. Pavadinimui pakeisti paspauskite ant pirmosios eilutės ir naudokite klaviatūrą pavadinimui pakeisti.</p>
    <p>Naudokite mygtuką <gui>tekstas</gui> teksto formativimui rašteliuose. Šis mygtukas rodo meniu su keliomis komandomis:</p>

    <list>
      <item><title>Atšaukti</title>
        <p><gui>Atšaukimo</gui> komanda panaikina raštelio pakeitimu esamame seanse. Atšaukimui klaviatūra naudokite klavišų kombinaciją <keyseq><key>Vald</key><key>Z</key></keyseq>.</p>
      </item>
      <item><title>Pakartoti</title>
        <p><gui>Pakartojimo</gui> komanda grąžina pakeitimus, kurie buvo pašalinti naudojant <gui>atšaukimo</gui> funkciją. Paskutiniam pakeitimui pakartoti klaviatūra naudokite klavišų kombinaciją <keyseq><key>Lyg2</key><key>Vald</key><key>Z</key></keyseq>.</p>
      </item>
      <item><title>Pusjuodis</title>
        <p>Norėdami padaryti tekstą pusjuodį pažymėkite jį iš meniu <gui>Tekstas</gui> pasirinkite <gui>Pusjuodis</gui> arba paspauskite <keyseq><key>Vald</key><key>B</key></keyseq>.</p>
      </item>
      <item><title>Kursyvas</title>
        <p>Norėdami padaryti tekstą kursyvu pažymėkite jį iš meniu <gui>Tekstas</gui> pasirinkite <gui>Kursyvas</gui> arba paspauskite <keyseq><key>Vald</key><key>I</key></keyseq>.</p>
     </item>
     <item><title>Perbrauktas</title>
       <p>Perbraukimo stilius linija perbrauks pažymėtą tekstą. Perbraukimui pridėti, pažymėkite tekstą ir pasirinkite punktą <gui>Perbrauktas</gui> ir meniu <gui>Tekstas</gui> arba paspauskite <keyseq><key>Vald</key><key>S</key></keyseq>.</p>
     </item>
     <item><title>Paryškintas</title>
       <p>Paryškintas stilius aplink tekstą nustato skirtingą foną. Paryškinimui pridėti pažymėkite tekstą ir pasirinkite punktą <gui>Paryškintas</gui> iš meniu <gui>Tekstas</gui> arba paspauskite <keyseq><key>Ctrl</key><key>H</key></keyseq>.</p>
     </item>
     <item><title>Šrifto dydis</title>
       <p>Yra šioje meniu dalyje keturi punktai: mažas, normalus, didelis ir milžiniškas. Kiekvieną jų yra konkretus šrifto dydis pažymėtam tekstui raštelyje. Šrifto dydžiui pakeisti pažymėkite jį ir pasirinkite iš teksto meniu punktą <gui>Mažas</gui>, <gui>Normalus</gui>, <gui>Didelis</gui> arba <gui>Milžiniškas</gui>.</p>
     </item>
     <item><title>Punktai</title>
       <p>Pasirinkite elementą <gui>Punktai</gui> iš teksto meniu punktų sąrašui pradėti. Jei žymiklis yra  punktų sąraše, elementai <gui>Padidinti įtrauką</gui> ir <gui>Sumažinti įtrauką</gui> bus leidžiami.</p>
       <p>Kai žymiklis yra punktų sąraše, pasirinkite elementą <gui>Padidinti įtrauką</gui> esamai eilutei paslinkti į dešinę arba <gui>Sumažinti įtrauką</gui> paslinkimui į kairę.</p>
       <p>Daugiau apie punktų sąrašus skaitykite <link xref="bulleted-lists"/>.</p>
     </item>
   </list>
   <p>Įrankinė lango viršuje turi paieškos mygtuką. Naudokite jį paieškai dabartiniame raštelyje. Maža paieškos juosta bus atverta virš raštelio. Paieškos juostai atverti klaviatūra naudokite klavišų kombinaciją <keyseq><key>Vald</key><key>F</key></keyseq>. Įveskite paieškos tekstą ir spauskite <key>Enter</key>, atitikmenys bus paryškinti. Spauskite <gui>Kitas</gui> tolesniam atitikmeniui paryškinti. Spauskite <gui>Ankstesnis</gui> perėjimui prie ankstesnio atitikmens. Paieškos juostai užverti spauskite paspauskite klavišą <key>Escape</key> arba paieškos mygtuką dar kartą.</p>

   <section id="note-content" style="2column">
     <title>Raiškusis raštelio turinys</title>
   </section>

</page>
