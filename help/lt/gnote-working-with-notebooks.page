<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="working-with-notebooks" xml:lang="lt">


  <info>
    <link type="guide" xref="index#organizing-notes"/>
    <desc>Raštelių tvarkymas</desc>
    <license><p>GNU laisva dokumentacijos licencija (GFDL), versija 1.1</p></license>
    <credit type="author">
      <name>Pierre-Yves Luyten</name>
      <years>2011</years>
    </credit>
    <credit type="editor">
      <name>Aurimas Cernius</name>
      <years>2011-2012,2019</years>
    </credit>
    <credit type="editor">
      <name>Adam Dingle</name>
      <email>adam@yorba.org</email>
      <years>2013</years>
    </credit>
  </info>

  <title>Darbas su užrašinėmis</title>
    <p>Užrašinės leidžia kartu sugrupuoti susijusius raštelius. Naują užrašinę galite pridėti pagrindiniame lange arba tiesiogiai raštelyje.</p>

    <section id="creating-notebooks"><title>Užrašinių kūrimas</title>
      <p>Norėdami sukurti naują užrašinę:</p>
      <steps>
        <item>
          <p>Spauskite meniu mygtuką pagrindiniame lange ir pasirinkite <gui>Nauja užrašinė</gui>.</p>
        </item>
        <item>
          <p>Įveskite naujos užrašinės pavadinimą.</p>
        </item>
      </steps>
      <p>Arba:</p>
      <steps>
        <item>
          <p>Raštelių apžvalgoje kairiajame polangyje spauskite dešinį klavišą ir pasirinkite <gui>Nauja...</gui>.</p>
        </item>
        <item>
          <p>Įveskite naujos užrašinės pavadinimą.</p>
        </item>
      </steps>
      <figure>
        <title>Nauja užrašinė iš kontekstinio meniu</title>
        <media type="image" src="figures/add-notebook-search.png" mime="image/png" style="right">
        <p>Nauja užrašinė</p>
      </media>
      </figure>  
    </section>

    <section id="adding-notes-to-a-notebook"><title>Raštelių pridėjimas į užrašinę</title>
      <p>Jei norite pridėti raštelius į užrašinę, tai galite padaryti pagrindiniame lange arba tiesiogiai raštelyje.</p>
      <p>Norėdami perkelti esamą raštelį pagrindiniame lange, atlikite vieną iš šių veiksmų:</p>
      <list>
        <item><p>Pele tempkite raštelį(-ius) į užrašinę kairėje.</p></item>
        <item><p>Spauskite ant užrašinės, tuomet spauskite dešinį klavišą raštelių sąraše ir kontekstiniame meniu pasirinkite <gui>Naujas</gui>. Bus sukurtas naujas raštelis ir pridėtas į užrašinę.</p></item>
      </list>
      <p>Rašydami raštelį galite pridėti jį į esamą užrašinę naudodami meniu punktą <guiseq><gui>Veiksmai</gui><gui>Užrašinė</gui></guiseq>.</p>
      <figure>
        <title>Raštelių pridėjimas į užrašinę</title>
        <media type="image" src="figures/add-to-notebook.png" mime="image/png" style="right"><p>Pridėti į užrašinę.</p></media>
      </figure>
      <p>Norėdami pašalinti raštelį iš užrašinės, atlikite vieną iš šių veiksmų:</p>
      <list>
        <item><p>Perkelkite raštelį į užrašinę <gui>Neįkelti rašteliai</gui> pagrindiniame lange.</p></item>
        <item><p>Pasirinkite meniu punktą <gui>Jokios užrašinės</gui>, kuri matoma paspaudus <guiseq><gui>Veiksmai</gui><gui>Užrašinė</gui></guiseq>.</p></item>
      </list>
    </section>

    <section id="creating-notebook-templates"><title>Užrašinės šablono sukūrimas</title>
      <p>Sukūrę užrašinę galite sukurti šabloną kiekvienam rašteliui, kurį sukursite toje užrašinėje.</p>
      <steps>
        <item><p>Pagrindiniame lange spauskite dešinį klavišą ant užrašinės ir pasirinkite <gui>Atverti šablono raštelį</gui>.</p>
        </item>
        <item><p>Bus atvertas naujas šablono raštelis. Bet koks šame raštelyje įrašytas tekstas bus įvestas visuose šioje užrašinėje sukuriamuose rašteliuose.</p></item>
      </steps>
      <figure>
        <title>Užrašinės šablono sukūrimas</title>
        <media type="image" src="figures/note-template.png" mime="image/png" style="right"><p>Raštelio šablonas.</p></media>
      </figure>
      <p>Daugiau rasite <link xref="gnote-template-notes"/>.</p>
    </section>

  <section id="deleting-a-notebook"><title>Užrašinės ištrynimas</title>
    <p>Užrašinės ištrynimas neištrins jokių joje esančių raštelių. Ištrynus užrašinę rašteliai nebus susieti su jokia užrašine ir bus matomi pagrindiniame lange pasirinkus <gui>Neįkelti rašteliai</gui>.</p>
    <p>Norėdami ištrinti užrašinę, eikite į paieškos veikseną pagrindiniame lange ir atlikite vieną iš šių veiksmų:</p>
    <list>
      <item><p>Spauskite dešinį klavišą ant užrašinės ir pasirinkite <gui>Ištrinti užrašinę</gui>.</p></item>
      <item><p>Pašalinkite visus raštelius iš užrašinės. Pati užrašinė bus automatiškai pašalinta iš naujo paleidus <app>Gnote</app>.</p></item>
    </list>
    <figure>
      <title>Raštelių ištrynimas pagrindiniame lange</title>
        <media type="image" src="figures/delete-notebook.png" mime="image/png" style="right">
          <p>Užrašinės ištrynimas</p>
        </media>
    </figure>
  </section>

  <section id="special-notebooks"><title>Specialiosios užrašinės</title>
    <p><app>Gnote</app> turi kelias specialias užrašines:</p>
    <list>
      <item><p><gui>Aktyvūs</gui> - turi šiame <app>Gnote</app> seanse atvertus raštelius.</p></item>
      <item><p><gui>Visi</gui> - turi visus raštelius, išskyrus šablonus.</p></item>
      <item><p><gui>Svarbūs</gui> - naudojamas rašteliams pažymėti svarbiais.</p></item>
      <item><p><gui>Neįkelti</gui> - turi raštelius, kurie nėra patalpinti jokioje naudotojo užrašinėje.</p></item>
    </list>
  </section>

</page>
