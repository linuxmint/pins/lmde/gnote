<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="searching-notes" xml:lang="de">

  <info>
    <link type="guide" xref="index#managing-notes"/>
    <license><p>GNU Free Documentation License (GFDL), Version 1.1</p></license>
    <desc>Notizen durchsuchen</desc>
    <credit type="author">
      <name>Pierre-Yves Luyten</name>
      <years>2011</years>
    </credit>
    <credit type="editor">
      <name>Aurimas Cernius</name>
      <years>2011-2013</years>
    </credit>
    <credit type="editor">
      <name>Adam Dingle</name>
      <email>adam@yorba.org</email>
      <years>2013</years>
    </credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jochen Skulj</mal:name>
      <mal:email>jochen@jochenskulj.de</mal:email>
      <mal:years>2008</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@gmail.com</mal:email>
      <mal:years>2009, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2009, 2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Winzen</mal:name>
      <mal:email>d@winzen4.de</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Überblick über Ihre Notizen</title>

  <p>Das Hauptfenster von <app>Gnote</app> zeigt eine Übersicht aller Ihrer Notizen an. Standardmäßig zeigt der Dialog die Notizen in der Reihenfolge an, in der sie zuletzt geändert worden sind. Klicken Sie auf die Spaltenüberschriften <gui>Notiz</gui> oder <gui>Zuletzt geändert</gui>, um die Sortierreihenfolge zu ändern. Durch ein zweites Klicken auf die Spaltenüberschrift kann zwischen einer aufsteigenden oder absteigenden Sortierreihenfolge gewechselt werden.</p>
  <p>Sie können bestimmte Notizen finden, indem Sie im Suchfeld einen Text eingeben. Die Liste der Notizen wird automatisch aktualisiert und zeigt nur die Notizen an, die dem Text entsprechen. Wenn die Notizliste fokussiert ist, können Sie einfach dort den Suchtext eintippen. Die Suche startet dann automatisch.</p>
  <p>Um eine Notiz zu öffnen, führen Sie eine der folgenden Aktionen aus:</p>
  <list>
    <item><p>Führen Sie einen Doppelklick auf der Notiz durch.</p></item>
    <item><p>Führen Sie einen Klick mit der rechten Maustaste in die Liste der Notizen aus und wählen Sie den Menüpunkt <gui>Öffnen</gui> aus dem Kontextmenü aus, das erscheint. Daraufhin werden alle gewählten Notizen jeweils in einem eigenen Fenster geöffnet.</p></item>
    <item><p>Führen Sie einen Klick mit der rechten Maustaste in die Liste der Notizen aus und wählen Sie den Menüpunkt <gui>In neuem Fenster öffnen</gui> aus dem Kontextmenü. Daraufhin werden alle gewählten Notizen jeweils in einem eigenen neuen Fenster geöffnet. Das ursprüngliche Fenster bleibt unberührt.</p></item>
    <item><p>Wählen Sie eine oder mehrere Notizen in einer Liste und drücken Sie anschließend <keyseq><key>Strg</key><key>O</key></keyseq>.</p></item>
    <item><p>Wenn das Statussymbol aktiviert ist, klicken Sie darauf und wählen Sie eine Notiz im erscheinenden Menü. Dieses Menü enthält nur eine begrenzte Anzahl Notizen und enthält angeheftete und zuletzt bearbeitete Notizen.</p></item>
  </list>

</page>
